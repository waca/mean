angular.module('MyApp', ['ngRoute', 'satellizer'])
	.config(function($routeProvider, $locationProvider, $authProvider) {
		$locationProvider.html5Mode(true);

		$routeProvider
			.when('/', {
				templateUrl: 'partials/home.html'
			})
			.when('/contact', {
				templateUrl: 'partials/contact.html',
				controller: 'ContactCtrl'
			})
			.when('/login', {
				templateUrl: 'partials/login.html',
				controller: 'LoginCtrl',
				resolve: { skipIfAuthenticated: skipIfAuthenticated }
			})
			.when('/signup', {
				templateUrl: 'partials/signup.html',
				controller: 'SignupCtrl',
				resolve: { skipIfAuthenticated: skipIfAuthenticated }
			})
			.when('/account', {
				templateUrl: 'partials/profile.html',
				controller: 'ProfileCtrl',
				resolve: { loginRequired: loginRequired }
			})
			.when('/:category/list/:page/:search',{
				templateUrl: 'partials/list.html',
				controller:'CategoryCtrl'
			})
			.when('/:category/view/:id',{
				templateUrl: 'partials/view.html',
				controller:'CategoryCtrl'
			})
			.when('/:category/write/:id',{
				templateUrl: 'partials/write.html',
				controller:'CategoryCtrl',
				resolve: { loginRequired: loginRequired }
			})
			.otherwise({
				templateUrl: 'partials/404.html'
			});

		$authProvider.loginUrl = '/login';
		$authProvider.signupUrl = '/signup';
		$authProvider.facebook({
			url: '/auth/facebook',
			clientId: '980220002068787',
			redirectUri: 'http://localhost:3000/auth/facebook/callback'
		});
		
		$authProvider.google({
			url: '/auth/google',
			clientId: '352969747374-dvocspc08kicvfkq6q8qk2fi168gr95r.apps.googleusercontent.com'
		});
		
		$authProvider.twitter({
			url: '/auth/twitter'
		});
	
		function skipIfAuthenticated($location, $auth) {
			if ($auth.isAuthenticated()) {
				$location.path('/');
			}
		}
	
		function loginRequired($location, $auth) {
			if (!$auth.isAuthenticated()) {
				$location.path('/login');
			}
		}
	
	}).run(function($rootScope, $window) {
		if ($window.localStorage.user) {
			$rootScope.currentUser = JSON.parse($window.localStorage.user);
		}
	});
