var express = require('express')
var router = express.Router();
'use strict';
//get
router.get('/:category/list/:page/:search',function(req,res,next){
	
});

router.get('/:category/view/:id',function(req,res,next){
	
});

router.get('/:category/write/:id',function(req,res,next){
	
});

//post
router.post('/:category/write',function(req,res,next){
	
});
//put
router.put('/:category/update/:id',function(req,res,next){
	
});
//delete
router.delete('/:category/:id',function(req,res,next){
	
});

module.exports = router;

