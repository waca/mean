var express = require('express')
var router = express.Router();
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
	service: 'Mailgun',
	auth: {
		user: process.env.MAILGUN_USERNAME,
		pass: process.env.MAILGUN_PASSWORD
	}
});

router.get('/conatat',function(req,res,next){
	"use strict";
	res.render('contact',{ title:'Contact' });
});


router.post('/contact',function(req,res){
	"use strict";
	req.assert('name', 'Name cannnot be blank').notEmpty();
	req.assert('email', 'Email is not valid').isEmail();
	req.assert('email', 'Email cannot be blank').notEmpty();
	req.assert('message', 'Message cannot be blank').notEmpty();
	req.sanitize('email').normalizeEmail({ remove_dots: false });

	var errors = req.validationErrors();

	if (errors) {
		return res.status(400).send(errors);
	}

	var mailOptions = {
		from: req.body.name + ' ' + '<'+ req.body.email + '>',
		to: 'tk5436@naver.com',
		subject: '누가 메일을 보냈어요.',
		text: req.body.message
	};

	transporter.sendMail(mailOptions, function(err) {
		res.send({ msg: 'Thank you! Your feedback has been submitted.' });
	});
});



module.exports = router;